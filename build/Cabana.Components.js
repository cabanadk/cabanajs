'use strict';
var _cabana = function () {

	this.auth = '0000';
	this.log = function() {
		console.log(this);
	};


	this.vars = {
		listeners: []
	};

};
var Cabana = new _cabana;


_cabana.prototype.Core = function (){
	
	this.init = function() {
		_cabana.prototype.on = this.on;
		_cabana.prototype.off = this.off;
		_cabana.prototype.trigger = this.trigger;
		
		this.listen();
	};

	this.on = function(event, listenerFn) {
		this.vars.listeners.push({
			event: event.toLowerCase(),
			callback: listenerFn
		});
	};

	this.off = function(event) {
		[].forEach.call(this.vars.listeners, function(listener, index) {
			if (listener.event == event.toLowerCase()) {
				this.vars.listeners.splice(listener, 1);
			}
		});
	};

	this.trigger = function(name) {
		var e;

		var eventName = name;

		console.log("eventName", eventName);

		if (document.createEvent) {
			e = document.createEvent('Event');
			e.initEvent(eventName, true, false);
		} else {
			e = document.createEventObject();
			e.eventType = eventName;
		}

		e.eventName = eventName;

		if (document.createElement) {
			document.dispatchEvent(e);
		} else {
			e.fireEvent('on'+eventName, e);
		}
	};

	this.listen = function() {
		document.addEventListener('', function(e) {
			console.log('listener', e);
		});
	};

	this.tracking = function(name) {
		if (!Cabana.vars.Share.tracking) {
			return;
		}

		if (typeof dataLayer !== 'undefined') {
			try {
				dataLayer.push({
					'event': name,
					'title': document.title ? document.title : '',
					'url': location.href
				})
			} catch(e) {
				console.error('Couldn\'t push to tag manager');
			}
		} else {
			this.fireEvent(type);
		}
	};

	return this;
};


